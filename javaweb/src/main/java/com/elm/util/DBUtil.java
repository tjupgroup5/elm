package com.elm.util;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

public class DBUtil {
    static String resource = "mybaits-config.xml";
    //private static SqlSession sessionFactory;
    private static ThreadLocal<SqlSession> tl = new ThreadLocal<>();

    //static 静态代码，在类加载的时候执行一次，且只执行一次
//    static {
//        //1 创建SqlSessionFactoryBuilder对象
//
//
//        InputStream inputStream = null;
//        try {
//            inputStream = Resources.getResourceAsStream(resource);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
//
////        获取sqlsession对象
//        SqlSession sqlSession = sqlSessionFactory.openSession();
//    }
    private static SqlSession creatSqlSession(){
        SqlSession sqlSession=null;
        if(sqlSession==null)
        {
            InputStream inputStream = null;
            try {
                inputStream = Resources.getResourceAsStream(resource);
            } catch (IOException e) {
                e.printStackTrace();
            }
            SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
            sqlSession = sqlSessionFactory.openSession();
        }
        return sqlSession;
    }
    public static SqlSession getSqlSession()
    {
        SqlSession sqlSession=null;
        sqlSession= tl.get();
        if(sqlSession==null)
        {
            sqlSession=creatSqlSession();
            tl.set(sqlSession);
        }
        return sqlSession;
    }
    public static  void beginTranscation() throws Exception{
        SqlSession sqlSession=null;
        sqlSession= tl.get();
        if(sqlSession==null)
        {
            sqlSession=creatSqlSession();
            tl.set(sqlSession);
        }

    }
    public static void commitTranscation()  throws Exception{
        SqlSession sqlSession=tl.get();
        sqlSession.commit();

    }
    public static void roolbackTranscation()  throws Exception{
        SqlSession sqlSession= tl.get();
        sqlSession.rollback();
    }

    public  static void  close(){
        SqlSession sqlSession= tl.get();
        if(sqlSession!=null)
        {
            sqlSession.close();
        }
        tl.remove();
    }
}


