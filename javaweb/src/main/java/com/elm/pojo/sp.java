package com.elm.pojo;

public class sp {
    public int cartId;
    public int foodId;

   public Integer businessId;
    public String userId;
    public Integer quantity;

    public Integer ffoodId;
    public String ffoodName;
    public String ffoodExplain;
    public String ffoodImg;
    public Double ffoodPrice;
    public Integer fbusinessId;

    public int getCartId() {
        return cartId;
    }

    public void setCartId(int cartId) {
        this.cartId = cartId;
    }

    public int getFoodId() {
        return foodId;
    }

    public void setFoodId(int foodId) {
        this.foodId = foodId;
    }

    public Integer getBusinessId() {
        return businessId;
    }

    public void setBusinessId(Integer businessId) {
        this.businessId = businessId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getFfoodId() {
        return ffoodId;
    }

    public void setFfoodId(Integer ffoodId) {
        this.ffoodId = ffoodId;
    }

    public String getFfoodName() {
        return ffoodName;
    }

    public void setFfoodName(String ffoodName) {
        this.ffoodName = ffoodName;
    }

    public String getFfoodExplain() {
        return ffoodExplain;
    }

    public void setFfoodExplain(String ffoodExplain) {
        this.ffoodExplain = ffoodExplain;
    }

    public String getFfoodImg() {
        return ffoodImg;
    }

    public void setFfoodImg(String ffoodImg) {
        this.ffoodImg = ffoodImg;
    }

    public Double getFfoodPrice() {
        return ffoodPrice;
    }

    public void setFfoodPrice(Double ffoodPrice) {
        this.ffoodPrice = ffoodPrice;
    }

    public Integer getFbusinessId() {
        return fbusinessId;
    }

    public void setFbusinessId(Integer fbusinessId) {
        this.fbusinessId = fbusinessId;
    }

    public String getFremarks() {
        return fremarks;
    }

    public void setFremarks(String fremarks) {
        this.fremarks = fremarks;
    }

    public Integer getBbusinessId() {
        return bbusinessId;
    }

    public void setBbusinessId(Integer bbusinessId) {
        this.bbusinessId = bbusinessId;
    }

    public String getBbusinessName() {
        return bbusinessName;
    }

    public void setBbusinessName(String bbusinessName) {
        this.bbusinessName = bbusinessName;
    }

    public String getBbusinessAddress() {
        return bbusinessAddress;
    }

    public void setBbusinessAddress(String bbusinessAddress) {
        this.bbusinessAddress = bbusinessAddress;
    }

    public String getBbusinessExplain() {
        return bbusinessExplain;
    }

    public void setBbusinessExplain(String bbusinessExplain) {
        this.bbusinessExplain = bbusinessExplain;
    }

    public String getBbusinessImg() {
        return bbusinessImg;
    }

    public void setBbusinessImg(String bbusinessImg) {
        this.bbusinessImg = bbusinessImg;
    }

    public Integer getBorderTypeId() {
        return borderTypeId;
    }

    public void setBorderTypeId(Integer borderTypeId) {
        this.borderTypeId = borderTypeId;
    }

    public double getBstarPrice() {
        return bstarPrice;
    }

    public void setBstarPrice(double bstarPrice) {
        this.bstarPrice = bstarPrice;
    }

    public double getBdeliveryPrice() {
        return bdeliveryPrice;
    }

    public void setBdeliveryPrice(double bdeliveryPrice) {
        this.bdeliveryPrice = bdeliveryPrice;
    }

    public String getBremarks() {
        return bremarks;
    }

    public void setBremarks(String bremarks) {
        this.bremarks = bremarks;
    }

    public String fremarks;

    public Integer bbusinessId;
    public String bbusinessName;
    public String bbusinessAddress;
    public String bbusinessExplain;
    public String bbusinessImg;
    public Integer borderTypeId;
    public double bstarPrice; //起送费
    public double bdeliveryPrice; //配送费
    public String bremarks;
}
