package com.elm.dao;

import com.elm.pojo.OrderDetailet;

import java.util.List;

public interface OrderDetailetDao {
    public int saveOrderDetailetBatch(List<OrderDetailet> list) throws Exception;
    public List<OrderDetailet> listOrderDetailetByOrderId(Integer orderId) throws Exception;
}
