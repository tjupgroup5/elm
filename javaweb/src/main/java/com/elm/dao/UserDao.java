package com.elm.dao;

import com.elm.pojo.JiFen;
import com.elm.pojo.User;

import java.util.Collection;

public interface UserDao {
    public User getUserByIdByPass(String userId, String  password) throws Exception;
    public Collection<JiFen> getjifen(String userId) throws Exception;
    public  int addjifen(String userId,double total) throws  Exception;
    public  int dejifen(int id) throws  Exception;
    public  int upjifen(int id,double num) throws  Exception;

    public int getUserById(String userId) throws Exception;
    public int saveUser(User user) throws Exception;


}
