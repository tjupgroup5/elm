package com.elm.dao;

import com.elm.pojo.Business;
import com.elm.pojo.Food;

import java.util.List;

public interface FoodDao {
    public List<Food> listFoodByBusinessId(int businessId);
}
