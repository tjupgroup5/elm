package com.elm.dao.impl;

import com.elm.dao.OrdersDao;
import com.elm.mapper.DeliveryAddressmapper;
import com.elm.mapper.Ordermapper;
import com.elm.pojo.Orders;
import com.elm.util.DBUtil;
import org.apache.ibatis.session.SqlSession;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OrdersDaoImpl implements OrdersDao {
    SqlSession sqlSession=null;
    @Override
    public int saveOrders(Orders orders) throws Exception {
        Orders abs=orders;
        int id=0;
        int result=0;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date();
            String today = formatter.format(date);
            sqlSession= DBUtil.getSqlSession();
            Ordermapper mapper = sqlSession.getMapper(Ordermapper.class);
            result = mapper.insert(abs, today);
            id=mapper.selectid();
            System.out.println("这是id+"+id);
            sqlSession.commit();
        }
        finally {

        }
        return id;
    }

    @Override
    public Orders getOrdersById(Integer orderId) throws Exception {
        Orders orders = null;
        try {
            sqlSession= DBUtil.getSqlSession();
            Ordermapper mapper = sqlSession.getMapper(Ordermapper.class);
            orders = mapper.select(orderId);


        }
        finally {

        }

        return orders;
    }

    @Override
    public List<Orders> listOrdersByUserId(String userId) throws Exception {
        List<Orders> list = new ArrayList<>();
        try {
            sqlSession= DBUtil.getSqlSession();
            Ordermapper mapper = sqlSession.getMapper(Ordermapper.class);
           list=mapper.selecttwo(userId);


        }
        finally {

        }
        return  list;
    }

    @Override
    public int updateOrders(Orders orders) throws Exception {
        int result=0;
        try {
            sqlSession= DBUtil.getSqlSession();
            Ordermapper mapper = sqlSession.getMapper(Ordermapper.class);
            result = mapper.update(orders);
            sqlSession.commit();

        }
        finally {

        }
        return result;
    }
}
