package com.elm.dao.impl;

import com.elm.dao.CollectionDao;
import com.elm.mapper.Collectionmapper;
import com.elm.mapper.DeliveryAddressmapper;
import com.elm.pojo.Business;
import com.elm.pojo.Collections;
import com.elm.util.DBUtil;
import org.apache.ibatis.session.SqlSession;

import java.util.ArrayList;
import java.util.List;

public class CollectionDaoImpl implements CollectionDao {
    SqlSession sqlSession=null;
    @Override
    public int saveCollection(Collections collection) throws Exception {
        int result=0;
        try {
            sqlSession= DBUtil.getSqlSession();
            Collectionmapper mapper = sqlSession.getMapper(Collectionmapper.class);
            result = mapper.save(collection);

            sqlSession.commit();
        }
        finally {

        }
        return result;
    }

    @Override
    public int deleteCollection(Collections collection) throws Exception {
        int result=0;
        try {
            sqlSession= DBUtil.getSqlSession();
            Collectionmapper mapper = sqlSession.getMapper(Collectionmapper.class);
            result = mapper.updata(collection);

            sqlSession.commit();
        }
        finally {

        }
        return result;
    }

    @Override
    public List<Business> listCollection(Collections collection) throws Exception {
        List list=new ArrayList();
        try {
            sqlSession= DBUtil.getSqlSession();
            Collectionmapper mapper = sqlSession.getMapper(Collectionmapper.class);
            System.out.println("userID"+collection.getUserId());
           list = mapper.list(collection);

        }
        finally {

        }
        return list;
    }

    @Override
    public int getCollection(Collections collection) throws Exception {
        int result=0;
        try {
            sqlSession= DBUtil.getSqlSession();
            Collectionmapper mapper = sqlSession.getMapper(Collectionmapper.class);
            result = mapper.select(collection);
            System.out.println("这个结果应该为0但是"+result+"id"+collection.getUserId()+collection.getBusinessId());

        }
        finally {

        }
        return result;
    }
}
