package com.elm.dao.impl;

import com.elm.dao.CartDao;
import com.elm.mapper.Cartmapper;
import com.elm.mapper.Foodmapper;
import com.elm.pojo.Cart;
import com.elm.util.DBUtil;
import org.apache.ibatis.session.SqlSession;

import java.util.ArrayList;
import java.util.List;

public class CartDaoImpl implements CartDao {
    SqlSession sqlSession=null;
    @Override
    public int saveCart(Cart cart) throws Exception {
        int result = 0;
        try {
            sqlSession = DBUtil.getSqlSession();
            Cartmapper mapper = sqlSession.getMapper(Cartmapper.class);
           result = mapper.add(cart.getFoodId(), cart.getBusinessId(), cart.getUserId());
           sqlSession.commit();
        } finally {

        }

        return result;
    }

    @Override

    public int updateCart(Cart cart) throws Exception {
        int result = 0;
        try {
            sqlSession = DBUtil.getSqlSession();
            Cartmapper mapper = sqlSession.getMapper(Cartmapper.class);
            result = mapper.updata(cart.getQuantity(),cart.getFoodId(), cart.getBusinessId(), cart.getUserId());
            sqlSession.commit();
        } finally {

        }

        return result;
    }

    @Override
    public int removeCart(Cart cart) throws Exception {
        int result = 0;
        try {
            sqlSession = DBUtil.getSqlSession();
            Cartmapper mapper = sqlSession.getMapper(Cartmapper.class);
           result=mapper.delete(cart);
            sqlSession.commit();
        } finally {

        }

        return result;
    }

    @Override
    public List<Cart> listCart(Cart cart) throws Exception {
        List list=new ArrayList();
        try {
            sqlSession = DBUtil.getSqlSession();
            Cartmapper mapper = sqlSession.getMapper(Cartmapper.class);
            list = mapper.list(cart.getUserId(), cart.getBusinessId());
        } finally {

        }



        return list;
    }
}
