package com.elm.dao.impl;

import com.elm.dao.FoodDao;
import com.elm.mapper.Foodmapper;
import com.elm.pojo.Food;
import com.elm.util.DBUtil;
import org.apache.ibatis.session.SqlSession;

import java.util.ArrayList;
import java.util.List;

public class FoodDaoImpl implements FoodDao {
    SqlSession sqlSession=null;
    @Override
    public List<Food> listFoodByBusinessId(int businessId) {
        List<Food> list=new ArrayList<>();
        try {
            sqlSession = DBUtil.getSqlSession();
            Foodmapper mapper = sqlSession.getMapper(Foodmapper.class);
            list= mapper.selectbybuid(businessId);
//            Businessmapper mapper = sqlSession.getMapper(Businessmapper.class);
//            list = mapper.selectbyordertypeid( orderTypeId);

        } finally {

        }

        return list;
    }
}
