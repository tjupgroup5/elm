package com.elm.dao.impl;

import com.elm.dao.UserDao;
import com.elm.mapper.DeliveryAddressmapper;
import com.elm.mapper.Usermapper;
import com.elm.pojo.JiFen;
import com.elm.pojo.User;
import com.elm.util.DBUtil;
import org.apache.ibatis.session.SqlSession;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class UserDaoImpl implements UserDao {
    SqlSession sqlSession=null;
    @Override
    public User getUserByIdByPass(String userId, String password) throws Exception {
        User user=null;

        try {
            sqlSession = DBUtil.getSqlSession();
            Usermapper mapper = sqlSession.getMapper(Usermapper.class);
           user = mapper.selectbyidandpassword(userId, password);

        } finally {

        }

        return user;
    }

    @Override
    public Collection<JiFen> getjifen(String userId) throws Exception {
       List list=null;
        try {
            sqlSession = DBUtil.getSqlSession();
            Usermapper mapper = sqlSession.getMapper(Usermapper.class);
            list = mapper.select(userId);

        } finally {

        }

        return list;
    }

    @Override
    public int addjifen(String userId,double total) throws Exception {
      int result=0;
        try {
            sqlSession = DBUtil.getSqlSession();
            Usermapper mapper = sqlSession.getMapper(Usermapper.class);
//            SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            formatter.setTimeZone(TimeZone.getTimeZone("GMT+8:00"));
            Date a = new Date(System.currentTimeMillis());
            String format = formatter.format(a);
           //Date date=formatter.parse(format);
            Timestamp createTime = new Timestamp(formatter.parse(format).getTime());
            System.out.println("dasfwef"+userId+"asd "+total+"  "+createTime);
            result = mapper.add(userId, total, createTime);
            sqlSession.commit();


        } finally {

        }

        return result;

    }

    @Override
    public int dejifen(int id) throws Exception {
        int result=0;
        try {
            sqlSession=DBUtil.getSqlSession();
            Usermapper mapper = sqlSession.getMapper(Usermapper.class);
            result = mapper.delete(id);
            sqlSession.commit();
        }
        finally {

        }
        return result;
    }

    @Override
    public int upjifen(int id,double num) throws Exception {
        int result=0;
        try {
            sqlSession=DBUtil.getSqlSession();
            Usermapper mapper = sqlSession.getMapper(Usermapper.class);
            result = mapper.updata(id,num);
            sqlSession.commit();
        }
        finally {

        }
        return result;
    }

    @Override
    public int getUserById(String userId) throws Exception {
        int result=0;
        try {
            sqlSession=DBUtil.getSqlSession();
            Usermapper mapper = sqlSession.getMapper(Usermapper.class);
           result=mapper.selectbyuserid(userId);
            sqlSession.commit();
        }
        finally {

        }
        return result;
    }

    @Override
    public int saveUser(User user) throws Exception {
        int result=0;
        try {
            sqlSession=DBUtil.getSqlSession();
            Usermapper mapper = sqlSession.getMapper(Usermapper.class);
            result=mapper.save(user);
            sqlSession.commit();
        }
        finally {

        }
        return result;
    }
}
