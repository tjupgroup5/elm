package com.elm.dao.impl;

import com.elm.dao.OrderDetailetDao;
import com.elm.mapper.Foodmapper;
import com.elm.mapper.OrderDetailetmapper;
import com.elm.pojo.Food;
import com.elm.pojo.OrderDetailet;
import com.elm.util.DBUtil;
import org.apache.ibatis.session.SqlSession;

import java.util.ArrayList;
import java.util.List;

public class OrderDetailetDaoImpl implements OrderDetailetDao {
    SqlSession sqlSession=null;
    @Override
    public int saveOrderDetailetBatch(List<OrderDetailet> list) throws Exception {
        int result=0;
        try {
            sqlSession = DBUtil.getSqlSession();
            OrderDetailetmapper mapper = sqlSession.getMapper(OrderDetailetmapper.class);
            result = mapper.insert(list);
            sqlSession.commit();
        } finally {

        }
        return result;
    }

    @Override
    public List<OrderDetailet> listOrderDetailetByOrderId(Integer orderId) throws Exception {
        List<OrderDetailet> list=new ArrayList<>();
        try {
            sqlSession = DBUtil.getSqlSession();
            OrderDetailetmapper mapper = sqlSession.getMapper(OrderDetailetmapper.class);
            list=mapper.select(orderId);

        } finally {

        }

        return list;
    }
}
