package com.elm.dao.impl;

import com.elm.dao.DeliveryAddressDao;
import com.elm.mapper.DeliveryAddressmapper;
import com.elm.pojo.DeliveryAddress;
import com.elm.util.DBUtil;
import org.apache.ibatis.session.SqlSession;

import java.util.ArrayList;
import java.util.List;

public class DeliveryAddressDaoImpl implements DeliveryAddressDao {
    SqlSession sqlSession=null;
    @Override
    public List<DeliveryAddress> listDeliveryAddressByUserId(String userId) throws Exception {
        List list=new ArrayList();
        try {
            sqlSession = DBUtil.getSqlSession();
            DeliveryAddressmapper mapper = sqlSession.getMapper(DeliveryAddressmapper.class);
            list = mapper.select(userId);

        } finally {

        }



        return list;
    }

    @Override
    public int saveDeliveryAddress(DeliveryAddress deliveryAddress) throws Exception {
        int result=0;
        try {
            sqlSession=DBUtil.getSqlSession();
            DeliveryAddressmapper mapper = sqlSession.getMapper(DeliveryAddressmapper.class);
            result=mapper.save(deliveryAddress);
            sqlSession.commit();
        }
        finally {

        }
        return result;
    }

    @Override
    public DeliveryAddress getDeliveryAddressById(Integer daId) throws Exception {
        DeliveryAddress deliveryAddress=null;
        try {
            sqlSession=DBUtil.getSqlSession();
            DeliveryAddressmapper mapper = sqlSession.getMapper(DeliveryAddressmapper.class);
             deliveryAddress = mapper.selectbydaId(daId);

        }
        finally {

        }
        return deliveryAddress;
    }

    @Override
    public int updateDeliveryAddress(DeliveryAddress deliveryAddress) throws Exception {
        int result=0;
        try {
            sqlSession=DBUtil.getSqlSession();
            DeliveryAddressmapper mapper = sqlSession.getMapper(DeliveryAddressmapper.class);
            result=mapper.updata(deliveryAddress);
            sqlSession.commit();
        }
        finally {

        }
        return result;
    }

    @Override
    public int removeDeliveryAddress(Integer daId) throws Exception {
        int result=0;
        try {
            sqlSession=DBUtil.getSqlSession();
            DeliveryAddressmapper mapper = sqlSession.getMapper(DeliveryAddressmapper.class);
            result=mapper.delete(daId);
            sqlSession.commit();
        }
        finally {

        }
        return result;
    }
}
