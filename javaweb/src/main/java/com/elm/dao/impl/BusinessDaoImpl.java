package com.elm.dao.impl;

import com.elm.dao.BusinessDao;
import com.elm.mapper.Businessmapper;
import com.elm.pojo.Business;
import com.elm.util.DBUtil;
import org.apache.ibatis.session.SqlSession;

import java.util.ArrayList;
import java.util.List;

public class BusinessDaoImpl implements BusinessDao {
    SqlSession sqlSession=null;
    @Override
    public List<Business> listBusinessByOrderTypeId(Integer orderTypeId) throws Exception {
        List<Business>list=new ArrayList<>();
        try {
            sqlSession = DBUtil.getSqlSession();

            Businessmapper mapper = sqlSession.getMapper(Businessmapper.class);
            list = mapper.selectbyordertypeid( orderTypeId);

        } finally {

        }

        return list;
    }

    @Override
    public Business getBusinessById(Integer businessId) {
       Business business;
        try {
            sqlSession = DBUtil.getSqlSession();

            Businessmapper mapper = sqlSession.getMapper(Businessmapper.class);
            business = mapper.selectbybusinessid( businessId);

        } finally {

        }

        return business;
    }

    @Override
    public List<Business> listBusinessByname(String name) throws Exception {
        List<Business>list=new ArrayList<>();
        try {
            sqlSession = DBUtil.getSqlSession();

            Businessmapper mapper = sqlSession.getMapper(Businessmapper.class);
            list = mapper.selectbyname( name);

        } finally {

        }

        return list;
    }
}
