package com.elm.dao;

import com.elm.pojo.Business;

import java.util.List;

public interface BusinessDao {
    public List<Business> listBusinessByOrderTypeId(Integer orderTypeId) throws Exception;
    public Business getBusinessById(Integer businessId) throws Exception;
    public List<Business> listBusinessByname(String name) throws Exception;
}
