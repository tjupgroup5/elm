package com.elm.service;

import com.elm.pojo.Cart;

import java.util.List;

public interface CartService {
    public int saveCart(Cart cart) throws Exception;
    public int updateCart(Cart cart) throws Exception;
    public int removeCart(Cart cart) throws Exception;
    public List<Cart> listCart(Cart cart) throws Exception;
}
