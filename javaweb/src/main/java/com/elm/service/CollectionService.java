package com.elm.service;

import com.elm.pojo.Business;
import com.elm.pojo.Collections;

import java.util.List;

public interface CollectionService {
    public int saveCollection(Collections collection) throws Exception;


    public int deleteCollection(Collections collection) throws Exception;



    public List<Business> listCollection(Collections collection) throws Exception;


    public int getCollection(Collections collection) throws Exception;
}
