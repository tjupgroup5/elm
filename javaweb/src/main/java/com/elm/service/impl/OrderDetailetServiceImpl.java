package com.elm.service.impl;

import com.elm.dao.OrderDetailetDao;
import com.elm.dao.impl.OrderDetailetDaoImpl;
import com.elm.pojo.Food;
import com.elm.pojo.OrderDetailet;
import com.elm.service.OrderDetailetService;
import com.elm.util.DBUtil;

import java.util.ArrayList;
import java.util.List;

public class OrderDetailetServiceImpl implements OrderDetailetService {
    @Override
    public int saveOrderDetailetBatch(List<OrderDetailet> list) throws Exception {
        int result=0;
        OrderDetailetDao orderDetailetDao=new OrderDetailetDaoImpl();
        try {
            result=orderDetailetDao.saveOrderDetailetBatch(list);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            DBUtil.close();
        }
        return result;
    }

    @Override
    public List<OrderDetailet> listOrderDetailetByOrderId(Integer orderId) throws Exception {
        return null;
    }
}
