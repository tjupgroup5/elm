package com.elm.service.impl;

import com.elm.dao.DeliveryAddressDao;
import com.elm.dao.impl.DeliveryAddressDaoImpl;
import com.elm.pojo.DeliveryAddress;
import com.elm.service.DeliveryAddressService;
import com.elm.util.DBUtil;

import java.util.ArrayList;
import java.util.List;

public class DeliveryAddressServiceImpl implements DeliveryAddressService {
    @Override
    public List<DeliveryAddress> listDeliveryAddressByUserId(String userId) throws Exception {
        List<DeliveryAddress> list = new ArrayList<>();
        DeliveryAddressDao de=new DeliveryAddressDaoImpl();
        try {
            list = de.listDeliveryAddressByUserId(userId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            DBUtil.close();
        }
        return list;
    }

    @Override
    public int saveDeliveryAddress(DeliveryAddress deliveryAddress) throws Exception {
        int result=0;
        DeliveryAddressDao de=new DeliveryAddressDaoImpl();
        try {
            result = de.saveDeliveryAddress(deliveryAddress);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            DBUtil.close();
        }
        return result;
    }

    @Override
    public DeliveryAddress getDeliveryAddressById(Integer daId) throws Exception {
       DeliveryAddress deliveryAddress=null;
       DeliveryAddressDao de=new DeliveryAddressDaoImpl();
        try {
            deliveryAddress = de.getDeliveryAddressById(daId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            DBUtil.close();
        }
        return deliveryAddress;

    }

    @Override
    public int updateDeliveryAddress(DeliveryAddress deliveryAddress) throws Exception {
        int result=0;
        DeliveryAddressDao de=new DeliveryAddressDaoImpl();
        try {
            result = de.updateDeliveryAddress(deliveryAddress);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            DBUtil.close();
        }
        return result;
    }

    @Override
    public int removeDeliveryAddress(Integer daId) throws Exception {
        int result=0;
        DeliveryAddressDao de=new DeliveryAddressDaoImpl();
        try {
            result = de.removeDeliveryAddress(daId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            DBUtil.close();
        }
        return result;
    }
}
