package com.elm.service.impl;

import com.elm.dao.CartDao;
import com.elm.dao.OrderDetailetDao;
import com.elm.dao.OrdersDao;
import com.elm.dao.impl.CartDaoImpl;
import com.elm.dao.impl.OrderDetailetDaoImpl;
import com.elm.dao.impl.OrdersDaoImpl;
import com.elm.pojo.Cart;
import com.elm.pojo.Food;
import com.elm.pojo.OrderDetailet;
import com.elm.pojo.Orders;
import com.elm.service.OrdersService;
import com.elm.util.DBUtil;

import java.util.ArrayList;
import java.util.List;

public class OrdersServiceImpl implements OrdersService {
    @Override
    public int createOrders(String userId, Integer businessId, Integer daId, Double orderTotal) {
        int result = 0;

        CartDao cartDao = new CartDaoImpl();
        OrdersDao ordersDao = new OrdersDaoImpl();
        OrderDetailetDao orderDetailetDao = new OrderDetailetDaoImpl();
        try {

            Cart cart = new Cart();
            cart.setUserId(userId);
            cart.setBusinessId(businessId);
            List<Cart> cartList = cartDao.listCart(cart);
            System.out.println("cart   "+cartList+"  "+cartList.size());
            Orders orders = new Orders();
            orders.setUserId(userId);
            orders.setBusinessId(businessId);
            orders.setDaId(daId);
            orders.setOrderTotal(orderTotal);

            result = ordersDao.saveOrders(orders);
            System.out.println(result+"sadasfsfas");
            List<OrderDetailet> orderDetailetList = new ArrayList();
            for(Cart c : cartList) {
                OrderDetailet od = new OrderDetailet();
                od.setOrderId(result);
                od.setFoodId(c.getFoodId());
                od.setQuantity(c.getQuantity());
                orderDetailetList.add(od);
                System.out.println(orderDetailetList);
                cartDao.removeCart(cart);
            }
            int i = orderDetailetDao.saveOrderDetailetBatch(orderDetailetList);

            System.out.println("adasfa"+i);


        } catch (Exception e) {
        result = 0;
        try {
            DBUtil.roolbackTranscation(); //回滚一个事务
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        e.printStackTrace();
    }finally {
        DBUtil.close();
    }
        return result;

    }

    @Override
    public Orders getOrdersById(Integer orderId) {
        Orders orders = null;

        OrdersDao ordersDao = new OrdersDaoImpl();
        OrderDetailetDao orderDetailetDao = new OrderDetailetDaoImpl();

        try {


            //1、根据订单ID查询订单信息（多对一：商家）
            orders = ordersDao.getOrdersById(orderId);

            //2、根据订单ID查询订单明细信息
            List<OrderDetailet> list = orderDetailetDao.listOrderDetailetByOrderId(orderId);
            orders.setList(list);

        }catch (Exception e) {

            e.printStackTrace();
        }finally {
            DBUtil.close();
        }
        return orders;
    }

    @Override
    public List<Orders> listOrdersByUserId(String userId) {
        List<Orders> list = new ArrayList<>();

        OrdersDao ordersDao = new OrdersDaoImpl();
        OrderDetailetDao orderDetailetDao = new OrderDetailetDaoImpl();

        try {


            //1、根据用户ID查询订单信息（多对一：商家）
            list = ordersDao.listOrdersByUserId(userId);

            //2、查询多个订单的订单明细信息
            for(Orders o : list) {
                List<OrderDetailet> odList =
                        orderDetailetDao.listOrderDetailetByOrderId(o.getOrderId());
                o.setList(odList);

            }

        }catch (Exception e) {
            e.printStackTrace();
        }finally {
            DBUtil.close();
        }
        return list;
    }

    @Override
    public int updateOrders(Orders orders) throws Exception {
        int result=0;
        OrdersDao ordersDao=new OrdersDaoImpl();
        try {
            result = ordersDao.updateOrders(orders);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            DBUtil.close();
        }
        return result;
    }

}
