package com.elm.service.impl;

import com.elm.dao.CartDao;
import com.elm.dao.CollectionDao;
import com.elm.dao.impl.CartDaoImpl;
import com.elm.dao.impl.CollectionDaoImpl;
import com.elm.pojo.Business;
import com.elm.pojo.Collections;
import com.elm.service.CollectionService;
import com.elm.util.DBUtil;

import java.util.ArrayList;
import java.util.List;

public class CollectionServiceImpl implements CollectionService {
    @Override
    public int saveCollection(Collections collection) throws Exception {
        int result = 0;
        CollectionDao collectionDao=new CollectionDaoImpl();
        try {
            result= collectionDao.saveCollection(collection);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            DBUtil.close();
        }
        return result;
    }

    @Override
    public int deleteCollection(Collections collection) throws Exception {
        int result = 0;
        CollectionDao collectionDao=new CollectionDaoImpl();
        try {
            result= collectionDao.deleteCollection(collection);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            DBUtil.close();
        }
        return result;
    }

    @Override
    public List<Business> listCollection(Collections collection) throws Exception {
        List list=new ArrayList();
        CollectionDao collectionDao=new CollectionDaoImpl();
        try {
           list= collectionDao.listCollection(collection);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            DBUtil.close();
        }
        return list;
    }

    @Override
    public int getCollection(Collections collection) throws Exception {
        int result = 0;
        CollectionDao collectionDao=new CollectionDaoImpl();
        try {
            result= collectionDao.getCollection(collection);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            DBUtil.close();
        }
        return result;
    }
}
