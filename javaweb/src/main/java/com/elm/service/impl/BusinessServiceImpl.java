package com.elm.service.impl;

import com.elm.dao.BusinessDao;
import com.elm.dao.impl.BusinessDaoImpl;
import com.elm.pojo.Business;
import com.elm.service.BusinessService;
import com.elm.util.DBUtil;

import java.util.ArrayList;
import java.util.List;

public class BusinessServiceImpl implements BusinessService {
    @Override
    public List<Business> listBusinessByOrderTypeId(Integer orderTypeId) {
        ArrayList<Business> list = new ArrayList<>();
        BusinessDao dao=new BusinessDaoImpl();
        try {
            list = (ArrayList<Business>) dao.listBusinessByOrderTypeId(orderTypeId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            DBUtil.close();
        }
        return list;
    }

    @Override
    public Business getBusinessById(Integer businessId) {
        Business business = null;
        BusinessDao dao=new BusinessDaoImpl();
        try {
           business = dao.getBusinessById(businessId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            DBUtil.close();
        }
        return business;


    }

    @Override
    public List<Business> listBusinessByname(String name) throws Exception {
        ArrayList<Business> list = new ArrayList<>();
        BusinessDao dao=new BusinessDaoImpl();
        try {
            list = (ArrayList<Business>) dao.listBusinessByname(name);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            DBUtil.close();
        }
        return list;
    }
}
