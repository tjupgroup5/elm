package com.elm.service.impl;

import com.elm.dao.UserDao;
import com.elm.dao.impl.UserDaoImpl;
import com.elm.pojo.JiFen;
import com.elm.pojo.User;
import com.elm.service.UserService;
import com.elm.util.DBUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class UserServiceImpl implements UserService {
    @Override
    public User getUserByIdByPass(String userId, String password) throws Exception {
        User user=null;
        UserDao userDao=new UserDaoImpl();
        try {
           user = userDao.getUserByIdByPass(userId, password);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            DBUtil.close();
        }
        return user;

    }

    @Override
    public List<JiFen> getjifen(String userId) throws Exception {
       List list=new ArrayList<JiFen>();
        UserDao userDao=new UserDaoImpl();
        try {
            list = (ArrayList) userDao.getjifen(userId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            DBUtil.close();
        }
        return list;
    }

    @Override
    public int addjifen(String userId, double total) throws Exception {
        int result=0;
        UserDao userDao=new UserDaoImpl();
        try {
            result = userDao.addjifen(userId, total);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            DBUtil.close();
        }
        return result;
    }

    @Override
    public int dejifen(int id) throws Exception {
        int result=0;
        UserDao userDao=new UserDaoImpl();
        try {
            result = userDao.dejifen(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            DBUtil.close();
        }
        return result;
    }

    @Override
    public int upjifen(int id, double num) throws Exception {
        int result=0;
        UserDao userDao=new UserDaoImpl();
        try {
            result = userDao.upjifen(id,num);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            DBUtil.close();
        }
        return result;
    }

    @Override
    public int getUserById(String userId) throws Exception {
        int result=0;
        UserDao userDao=new UserDaoImpl();
        try {
            result = userDao.getUserById(userId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            DBUtil.close();
        }
        return result;
    }

    @Override
    public int saveUser(User user) throws Exception {
        int result=0;
        UserDao userDao=new UserDaoImpl();
        try {
            result = userDao.saveUser(user);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            DBUtil.close();
        }
        return result;
    }
}
