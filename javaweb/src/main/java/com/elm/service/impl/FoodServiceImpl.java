package com.elm.service.impl;

import com.elm.dao.FoodDao;
import com.elm.dao.impl.FoodDaoImpl;
import com.elm.pojo.Business;
import com.elm.pojo.Food;
import com.elm.service.FoodService;
import com.elm.util.DBUtil;

import java.util.ArrayList;
import java.util.List;

public class FoodServiceImpl implements FoodService {
    @Override
    public List<Food> listFoodByBusinessId(int businessId) throws Exception {
        ArrayList<Food> list = new ArrayList<>();
        FoodDao foodDao=new FoodDaoImpl();
        try {
            list = (ArrayList<Food>) foodDao.listFoodByBusinessId(businessId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            DBUtil.close();
        }
        return list;
    }
}
