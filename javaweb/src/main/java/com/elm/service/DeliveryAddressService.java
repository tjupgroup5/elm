package com.elm.service;

import com.elm.dao.DeliveryAddressDao;
import com.elm.pojo.DeliveryAddress;

import java.util.List;

public interface DeliveryAddressService {
    public List<DeliveryAddress> listDeliveryAddressByUserId(String userId) throws Exception;
    public int saveDeliveryAddress(DeliveryAddress deliveryAddress) throws Exception;
    public DeliveryAddress getDeliveryAddressById(Integer daId) throws Exception;
    public int updateDeliveryAddress(DeliveryAddress deliveryAddress) throws Exception;
    public int removeDeliveryAddress(Integer daId) throws Exception;
}
