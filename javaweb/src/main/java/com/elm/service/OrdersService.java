package com.elm.service;

import com.elm.pojo.Orders;

import java.util.List;

public interface OrdersService {
    public int createOrders(String userId,Integer businessId,Integer daId,Double orderTotal);
    public Orders getOrdersById(Integer orderId);
    public List<Orders> listOrdersByUserId(String userId);
    public int updateOrders(Orders orders) throws Exception ;
}
