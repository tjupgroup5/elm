package com.elm.service;

import com.elm.pojo.OrderDetailet;

import java.util.List;

public interface OrderDetailetService {
    public int saveOrderDetailetBatch(List<OrderDetailet> list) throws Exception;
    public List<OrderDetailet> listOrderDetailetByOrderId(Integer orderId) throws Exception;
}
