package com.elm.service;

import com.elm.pojo.Food;

import java.util.List;

public interface FoodService {
    public List<Food> listFoodByBusinessId(int businessId) throws Exception;
}
