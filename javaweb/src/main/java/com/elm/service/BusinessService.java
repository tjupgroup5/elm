package com.elm.service;

import com.elm.pojo.Business;

import java.util.List;

public interface BusinessService {
    public List<Business> listBusinessByOrderTypeId(Integer orderTypeId);
    public Business getBusinessById(Integer businessId);
    public List<Business> listBusinessByname(String name) throws Exception;
}
