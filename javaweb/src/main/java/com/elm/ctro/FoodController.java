package com.elm.ctro;

import com.elm.pojo.Food;
import com.elm.service.FoodService;
import com.elm.service.impl.FoodServiceImpl;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class FoodController {
    public Object listFoodByBusinessId(HttpServletRequest req) throws Exception{
        System.out.println(Integer.valueOf(req.getParameter("businessId")));
        Integer businessId = Integer.valueOf(req.getParameter("businessId"));

        FoodService foodService = new FoodServiceImpl();
        List<Food> list= foodService.listFoodByBusinessId(businessId);
        return  list;
    }
}
