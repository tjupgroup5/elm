package com.elm.ctro;

import com.elm.dao.DeliveryAddressDao;
import com.elm.pojo.Cart;
import com.elm.pojo.DeliveryAddress;
import com.elm.service.CartService;
import com.elm.service.DeliveryAddressService;
import com.elm.service.impl.CartServiceImpl;
import com.elm.service.impl.DeliveryAddressServiceImpl;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class DeliveryAddressController {
    public Object listDeliveryAddressByUserId(HttpServletRequest req) throws Exception{
        String userId = req.getParameter("userId");
        DeliveryAddressService service = new DeliveryAddressServiceImpl();
        List<DeliveryAddress> list= service.listDeliveryAddressByUserId(userId);
        return list;

    }
    public Object saveDeliveryAddress(HttpServletRequest req) throws Exception
    {
        DeliveryAddress deliveryAddress = new DeliveryAddress();
        deliveryAddress.setContactName(req.getParameter("contactName"));
        deliveryAddress.setContactSex(Integer.valueOf(req.getParameter("contactSex")));
        deliveryAddress.setContactTel(req.getParameter("contactTel"));
        deliveryAddress.setAddress(req.getParameter("address"));
        deliveryAddress.setUserId(req.getParameter("userId"));
        DeliveryAddressService service = new DeliveryAddressServiceImpl();
        int result = service.saveDeliveryAddress(deliveryAddress);
        return result;
    }
    public Object getDeliveryAddressById(HttpServletRequest req) throws Exception{
        Integer daId = Integer.valueOf(req.getParameter("daId"));
        DeliveryAddressService service = new DeliveryAddressServiceImpl();
        DeliveryAddress deliveryAddress = service.getDeliveryAddressById(daId);
        return deliveryAddress;
    }
    public Object updateDeliveryAddress(HttpServletRequest req) throws Exception{
        DeliveryAddress deliveryAddress = new DeliveryAddress();
        deliveryAddress.setContactName(req.getParameter("contactName"));
        deliveryAddress.setContactSex(Integer.valueOf(req.getParameter("contactSex")));
        deliveryAddress.setContactTel(req.getParameter("contactTel"));
        deliveryAddress.setAddress(req.getParameter("address"));
        deliveryAddress.setDaId(Integer.valueOf(req.getParameter("daId")));
        DeliveryAddressService service = new DeliveryAddressServiceImpl();
        int result = service.updateDeliveryAddress(deliveryAddress);
        return result;
    }
    public Object removeDeliveryAddress(HttpServletRequest req) throws Exception{
        Integer daId = Integer.valueOf(req.getParameter("daId"));
        DeliveryAddressService service = new DeliveryAddressServiceImpl();
        int result = service.removeDeliveryAddress(daId);

        return result;
    }
}
