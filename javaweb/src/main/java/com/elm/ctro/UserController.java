package com.elm.ctro;

import com.elm.pojo.JiFen;
import com.elm.pojo.User;
import com.elm.service.UserService;
import com.elm.service.impl.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class UserController {
    public Object getUserByIdByPass(HttpServletRequest req) throws Exception{
        String userId = req.getParameter("userId");
        String password = req.getParameter("password");
        UserService userService=new UserServiceImpl();
        User userByIdByPass = userService.getUserByIdByPass(userId, password);
        return userByIdByPass;


    }
    public Object getjifen(HttpServletRequest req) throws Exception{
        String userId = req.getParameter("userId");
        System.out.println("用户id"+userId);
         double sum=0;
        UserService userService=new UserServiceImpl();
        List<JiFen> list = userService.getjifen(userId);
        System.out.println("listzheshi"+list);
        for (JiFen jiFen : list) {
//            String time= jiFen.getTime();
//
//            SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            Date date=formatter.parse(time);
//            Date a = new Date(System.currentTimeMillis());
//            long l = a.getTime() - date.getTime();
            SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            formatter.setTimeZone(TimeZone.getTimeZone("GMT+8:00"));
            Timestamp time = jiFen.getTime();
            System.out.println(time+"id是"+jiFen.getId());
            Date date= new Date(time.getTime());
            Date a = new Date(System.currentTimeMillis());
            String format = formatter.format(a);
            Date parse = formatter.parse(format);
            long l = parse.getTime() - date.getTime();
            if((l/(1000*60*60*24))<31){
                sum+=jiFen.getJifen();
            }
        }
        System.out.println("jif"+sum);
        DecimalFormat df = new DecimalFormat("#.00");
        return df.format(sum);


    }
    public Object addjifen(HttpServletRequest req) throws Exception{
        String userId = req.getParameter("userId");
        Double total = Double.valueOf(req.getParameter("total"));
        System.out.println("id是"+userId);
        System.out.println("adafweq");

        System.out.println("积分是"+total);
        UserService userService=new UserServiceImpl();
        int addjifen = userService.addjifen(userId, total);
        return addjifen;
    }
    public Object updatajifen(HttpServletRequest req) throws Exception {
        String userId = req.getParameter("userId");
        Double use = Double.valueOf(req.getParameter("use"));
        System.out.println(use);
        UserService userService=new UserServiceImpl();
        List<JiFen> list = userService.getjifen(userId);
        for (JiFen jiFen : list) {
            int id=jiFen.getId();
            Double jifen = jiFen.getJifen();
            if(use>jifen)
            {
                use=use-jifen;
                int updatajifen = userService.dejifen(id);


            }else {
                jifen=jifen-use;
                userService.upjifen(id,jifen);
            }
        }
        return null;
    }
    public Object getUserById(HttpServletRequest request) throws Exception{
        String userId = request.getParameter("userId");
        UserService service = new UserServiceImpl();
        int result = service.getUserById(userId);
        return result;
    }

    public Object saveUser(HttpServletRequest request) throws Exception{
        User user = new User();
        user.setUserId(request.getParameter("userId"));
        user.setPassword(request.getParameter("password"));
        user.setUserName(request.getParameter("userName"));
        user.setUserSex(Integer.valueOf(request.getParameter("userSex")));
        UserService service = new UserServiceImpl();
        int result = service.saveUser(user);
        return result;
    }
}
