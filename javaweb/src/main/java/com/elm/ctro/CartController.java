package com.elm.ctro;

import com.elm.dao.CartDao;
import com.elm.dao.impl.CartDaoImpl;
import com.elm.pojo.Cart;
import com.elm.service.CartService;
import com.elm.service.impl.CartServiceImpl;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

public class CartController {
    public Object saveCart(HttpServletRequest req) throws Exception {
        Cart cart = new Cart();
        cart.setFoodId(Integer.valueOf(req.getParameter("foodId")));
        cart.setBusinessId(Integer.valueOf(req.getParameter("businessId")));
        cart.setUserId(req.getParameter("userId"));
        CartService cartService = new CartServiceImpl();
        int i = cartService.saveCart(cart);
        return i;
    }

    public Object updateCart(HttpServletRequest req) throws Exception {
        Cart cart = new Cart();
        cart.setFoodId(Integer.valueOf(req.getParameter("foodId")));
        cart.setBusinessId(Integer.valueOf(req.getParameter("businessId")));
        cart.setUserId(req.getParameter("userId"));
        CartService cartService = new CartServiceImpl();
        cart.setQuantity(Integer.valueOf(req.getParameter("quantity")));
        int i = cartService.updateCart(cart);
        return i;
    }
    public Object removeCart(HttpServletRequest request) throws Exception{
        Cart cart = new Cart();
        cart.setFoodId(Integer.valueOf(request.getParameter("foodId")));
        cart.setBusinessId(Integer.valueOf(request.getParameter("businessId")));
        cart.setUserId(request.getParameter("userId"));
        CartService service = new CartServiceImpl();
        int result = service.removeCart(cart);
        return result;

    }
    public Object listCart(HttpServletRequest request) throws Exception {
        Cart cart = new Cart();
        cart.setUserId(request.getParameter("userId"));
        if (request.getParameter("businessId") != null) {
            cart.setBusinessId(Integer.valueOf(request.getParameter("businessId")));
        }
        CartService service = new CartServiceImpl();
        List<Cart> list = service.listCart(cart);
        return list;
    }
    }
