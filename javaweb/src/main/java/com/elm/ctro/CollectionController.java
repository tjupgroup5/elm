package com.elm.ctro;

import com.elm.pojo.Business;
import com.elm.pojo.Collections;
import com.elm.service.CollectionService;
import com.elm.service.impl.CollectionServiceImpl;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class CollectionController {
    public Object saveCollection(HttpServletRequest req) throws Exception{
        Collections collections=new Collections();
        collections.setBusinessId(Integer.valueOf(req.getParameter("businessId")));
        collections.setUserId(req.getParameter("userId"));
        CollectionService col=new CollectionServiceImpl();
        int i = col.saveCollection(collections);
        return i;
    }
    public Object deleteCollection(HttpServletRequest req) throws Exception{
        Collections collections=new Collections();
        collections.setBusinessId(Integer.valueOf(req.getParameter("businessId")));
        collections.setUserId(req.getParameter("userId"));
        CollectionService col=new CollectionServiceImpl();
        int i = col.deleteCollection(collections);
        return i;
    }
    public Object listCollection(HttpServletRequest req) throws Exception{
        Collections collections=new Collections();

        collections.setUserId(req.getParameter("userId"));

        CollectionService col=new CollectionServiceImpl();
        List<Business> businesses = col.listCollection(collections);
        return businesses;
    }
    public Object getCollection(HttpServletRequest req) throws Exception{
        Collections collections=new Collections();
        collections.setBusinessId(Integer.valueOf(req.getParameter("businessId")));
        collections.setUserId(req.getParameter("userId"));
        CollectionService col=new CollectionServiceImpl();
        int i = col.getCollection(collections);
        return i;
    }

}
