package com.elm.ctro;

import com.elm.pojo.Business;
import com.elm.service.BusinessService;
import com.elm.service.impl.BusinessServiceImpl;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class BusinessController {
    public Object listBusinessByOrderTypeId(HttpServletRequest req) throws Exception {

        Integer orderTypeId =Integer.valueOf(req.getParameter("orderTypeId"));
        System.out.println(orderTypeId);
        BusinessService businessService = new BusinessServiceImpl();
        List<Business> list = businessService.listBusinessByOrderTypeId(orderTypeId);

        return list;
    }

    public Object getBusinessById(HttpServletRequest req) throws Exception{
        Integer businessId = Integer.valueOf(req.getParameter("businessId"));
        BusinessService service = new BusinessServiceImpl();
        Business business = service.getBusinessById(businessId);
        return business;
    }
    public Object listBusinessByname(HttpServletRequest req) throws Exception {

        String name = req.getParameter("Uname");
        System.out.println("asda"+name);
        BusinessService businessService = new BusinessServiceImpl();
        List<Business> list = businessService.listBusinessByname(name);

        return list;
    }
    

}
