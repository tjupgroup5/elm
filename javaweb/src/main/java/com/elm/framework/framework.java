package com.elm.framework;

import com.elm.ctro.BusinessController;
import com.elm.ctro.FoodController;
import org.codehaus.jackson.map.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;

@WebServlet("/")
public class framework extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setCharacterEncoding("UTF-8");

        resp.setCharacterEncoding("utf-8");
        resp.setContentType("application/json;charset=utf-8");

        String servletPath = req.getServletPath();
        String className = servletPath.substring(1, servletPath.lastIndexOf("/"));
        String methondName = servletPath.substring(servletPath.lastIndexOf("/") + 1);
        System.out.println(servletPath+" "+className+" "+methondName);
//        PrintWriter writer = resp.getWriter();
        PrintWriter writer=null;
        try {
            System.out.println(className);
            Class<?> aClass = Class.forName("com.elm.ctro."+className);
            System.out.println(aClass);
            Object con = aClass.newInstance();
            //System.out.println(con instanceof FoodController);
            Method method = aClass.getMethod(methondName,HttpServletRequest.class);
            Object result = method.invoke(con, req);
            System.out.println( "aa:   "+result);

            writer = resp.getWriter();
            ObjectMapper objectMapper = new ObjectMapper();
            writer.print(objectMapper.writeValueAsString(result));
        } catch (Exception e) {
            //e.printStackTrace();
            System.out.println(("请求url:"+servletPath+" 类名 "+className+" 方法名"+methondName));
        }
        finally {
            writer.close();
        }

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doGet(req,resp);
    }
}
