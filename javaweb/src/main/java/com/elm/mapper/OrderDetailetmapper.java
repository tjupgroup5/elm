package com.elm.mapper;

import com.elm.pojo.OrderDetailet;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderDetailetmapper {
    int insert(@Param("list") List<OrderDetailet> list);
    List<OrderDetailet> select(@Param("orderId")Integer orderId);
}

