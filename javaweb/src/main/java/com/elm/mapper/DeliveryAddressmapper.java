package com.elm.mapper;

import com.elm.pojo.DeliveryAddress;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DeliveryAddressmapper {
    List<DeliveryAddress> select(@Param("UserId") String  UserId);
    Integer save(@Param("deliveryAddress") DeliveryAddress deliveryAddress);
    DeliveryAddress selectbydaId(@Param("daId") int daId);
    int updata(@Param("deliveryAddress") DeliveryAddress deliveryAddress);
    int delete(@Param("daId") Integer daId);
}
