package com.elm.mapper;

import com.elm.pojo.JiFen;
import com.elm.pojo.User;
import org.apache.ibatis.annotations.Param;

import java.sql.Timestamp;
import java.util.List;

public interface Usermapper {
User selectbyidandpassword(@Param("userId") String userId, @Param("password") String  password );
List<JiFen>select(@Param("userId") String userId);
int add(@Param("userId") String userId,@Param("total") double total,@Param("time") Timestamp time);
int delete(@Param("id") int id);

int updata(@Param("id") int id,@Param("num") double num);

int selectbyuserid(@Param("userId") String userId);
int save(@Param("user") User user);
}
