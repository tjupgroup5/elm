package com.elm.mapper;

import com.elm.pojo.Business;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface Businessmapper {
List<Business> selectbyordertypeid(@Param("id")int id);
Business selectbybusinessid(@Param("bid") int id);
    List<Business> selectbyname(@Param("name")String name);
}
