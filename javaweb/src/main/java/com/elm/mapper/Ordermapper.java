package com.elm.mapper;

import com.elm.pojo.Orders;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface Ordermapper {
    Integer selectid();
    int insert(@Param("orders") Orders orders, @Param("date") String date);
    Orders select(@Param("orderId") Integer orderId);
    List<Orders> selecttwo(@Param("userId") String userId);

    int update(@Param("orders") Orders orders);
}
