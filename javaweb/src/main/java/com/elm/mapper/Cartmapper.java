package com.elm.mapper;

import com.elm.pojo.Cart;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface Cartmapper {

    int add(@Param("foodId") int  foodId,@Param("businessId") int businessId,@Param("userId") String  userId);
    int updata(@Param("quantity") int quantity,@Param("foodId") int  foodId,@Param("businessId") int businessId,@Param("userId") String  userId);
    int delete(@Param("cart") Cart cart);
    List<Cart> list(@Param("userId")  String userId, @Param("businessId") Integer businessId );
}
