package com.elm.mapper;

import com.elm.pojo.Business;
import com.elm.pojo.Collections;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface Collectionmapper {
    int save(@Param("collections") Collections collection);
    int updata(@Param("collections") Collections collection);
    List<Business>list(@Param("collections") Collections collection);
    int select(@Param("collections") Collections collection);
}
