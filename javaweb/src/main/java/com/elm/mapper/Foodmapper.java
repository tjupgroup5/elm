package com.elm.mapper;

import com.elm.pojo.Food;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface Foodmapper {
    List<Food> selectbybuid(@Param("buid") int buid);
}
