window.onload = function(){
	//获取显示隐藏按钮的DOM数组
	let showBtnArr = document.getElementsByClassName('fa-caret-down');//通过类样式名获得
	//获取订单明细DOM数组
	let detailedBoxArr = document.getElementsByClassName('order-detailed');
	
	//设置默认所有订单明细都隐藏
	for(let i = 0; i < showBtnArr.length; i++){
		detailedBoxArr[i].style.display='none';
	}
	
	//遍历按钮DOM数组，对每一个元素写onclick事件，判断此按钮的订单明细是显示还是隐藏
	for(let i = 0; i < showBtnArr.length; i++){
		showBtnArr[i].onclick = function(){
			//判断订单明细DOM对象是否隐藏，如果是就显示，否则就隐藏
			if(detailedBoxArr[i].style.display == 'none'){
				detailedBoxArr[i].style.display = 'block';
			}else{
				detailedBoxArr[i].style.display = 'none';
			}
		}
	}
}
