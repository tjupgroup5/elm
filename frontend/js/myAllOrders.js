window.onload = function() {
	document.onscroll = function() {
		//获取滚动条位置
		let s1 = document.documentElement.scrollTop;
		let s2 = document.body.scrollTop;
		let scroll = s1 == 0 ? s2 : s1;
		//获取视口宽度
		let width = document.documentElement.clientWidth;
		//获取顶部固定块
		let search = document.getElementById('fixedBox');
		//判断滚动条超过视口宽度的12%时(header高度为12vw)，搜索块变成固定定位
		if (scroll > width * 0.12) {
			search.style.position = 'fixed';
			search.style.left = '0';
			search.style.top = '0';
		} else {
			search.style.position = 'static';
		}
	}
	
	
	//获取显示隐藏按钮的DOM对象
	let showBtnn = document.getElementById('showBtnn');
	//获取订单明细DOM对象
	let detailedBoxx  = document.getElementById('detailedBoxx');
	//设置默认订单明细为隐藏状态
	detailedBoxx.style.display='none';
	
	showBtnn.onclick = function(){
		//判断订单明细DOM对象是否隐藏
		if(detailedBoxx.style.display=='none'){
			detailedBoxx.style.display='block';
		}else {detailedBoxx.style.display='none';}	
	}
	//获取显示隐藏按钮的DOM对象
	let showBtnnn = document.getElementById('showBtnnn');
	//获取订单明细DOM对象
	let detailedBoxxx  = document.getElementById('detailedBoxxx');
	//设置默认订单明细为隐藏状态
	detailedBoxxx.style.display='none';
	
	showBtnnn.onclick = function(){
		//判断订单明细DOM对象是否隐藏
		if(detailedBoxxx.style.display=='none'){
			detailedBoxxx.style.display='block';
		}else {detailedBoxxx.style.display='none';}
	}
}
