package com.neusoft.elmboot.mapper;
import com.neusoft.elmboot.po.JiFen;
import org.apache.ibatis.annotations.*;
import com.neusoft.elmboot.po.User;

import java.util.List;

@Mapper
public interface UserMapper {
    @Select("select * from user where userId=#{userId} and password=#{password}")
    public User getUserByIdByPass(User user);

    @Select("select count(*) from user where userId=#{userId}")
    public int getUserById(String userId);

    @Insert("insert into user values(#{userId},#{password},#{userName},#{userSex},null,1)")
    public int saveUser(User user);
    @Select("select * from jifen where userId=#{userId}")
    public List<JiFen> getjifen(String userId);

    @Insert("insert into jifen values (#{userId},#{Time},#{total})")
    public int addjifen(JiFen jiFen);
    @Update("update jifen set jifen=#{num} where id=#{id}")
    public int updatejifen(JiFen jiFen);
    @Delete("delete  from jifen where id=#{id}")
    public int deletejifen(int id);
}
