package com.neusoft.elmboot.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.neusoft.elmboot.po.Business;
import com.neusoft.elmboot.po.Collections;

@Mapper
public interface CollectionMapper {

    @Insert("insert into collections(userId,businessId,delTag) values(#{userId},#{businessId},0)")
    @Options(useGeneratedKeys = true, keyProperty = "collectId", keyColumn = "collectId")
    public int saveCollection(Collections collection);
    @Update("update collections set delTag = 1 where userId = #{userId} and businessId = #{businessId}")
    public int deleteCollection(Collections collection);
    @Select("select * from business where businessId in (select businessId from collections where userId = #{userId} and delTag = 0)")
    public List<Business> listCollection(String userId);
    @Select("select count(*) from collections where userId = #{userId} and businessId = #{businessId} and delTag = 0")
    public int getCollection(Collections collection);
}