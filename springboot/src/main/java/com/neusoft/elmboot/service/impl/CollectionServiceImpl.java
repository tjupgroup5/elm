package com.neusoft.elmboot.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.neusoft.elmboot.mapper.CollectionMapper;
import com.neusoft.elmboot.po.Business;
import com.neusoft.elmboot.po.Collections;
import com.neusoft.elmboot.service.CollectionService;

@Service
public class CollectionServiceImpl implements CollectionService{

    @Autowired
    private CollectionMapper collectionMapper;

    @Override
    @Transactional
    //用户收藏商家
    public int saveCollection(Collections collection) {
        collectionMapper.saveCollection(collection);
        int collectId = collection.getCollectId();
        return collectId;
    }

    //取消收藏
    @Override
    public int deleteCollection(Collections collection) {
        return collectionMapper.deleteCollection(collection);
    }

    //返回当前用户所有收藏的商家
    @Override
    public List<Business> listCollection(String userId) {
        return collectionMapper.listCollection(userId);
    }

    //判断是否收藏商家
    @Override
    public int getCollection(Collections collection) {
        return collectionMapper.getCollection(collection);
    }
}
