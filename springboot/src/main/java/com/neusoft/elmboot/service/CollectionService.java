package com.neusoft.elmboot.service;

import java.util.List;

import com.neusoft.elmboot.po.Business;
import com.neusoft.elmboot.po.Collections;

public interface CollectionService {

    public int saveCollection(Collections collection);

    public int deleteCollection(Collections collection);

    public List<Business> listCollection(String userId);

    public int getCollection(Collections collection);
}
