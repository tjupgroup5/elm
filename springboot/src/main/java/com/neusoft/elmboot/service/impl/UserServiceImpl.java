package com.neusoft.elmboot.service.impl;
import com.neusoft.elmboot.po.JiFen;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.neusoft.elmboot.mapper.UserMapper;
import com.neusoft.elmboot.po.User;
import com.neusoft.elmboot.service.UserService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserMapper userMapper;
    @Override
    public User getUserByIdByPass(User user) {
        return userMapper.getUserByIdByPass(user);
    }

    @Override
    public int getUserById(String userId) {
        return userMapper.getUserById(userId);
    }

    @Override
    public int saveUser(User user) {
        return userMapper.saveUser(user);
    }
    @Override
    public int getjifen(String userId){
        List<JiFen> list = userMapper.getjifen(userId);
        int sum = 0;
        for (JiFen jiFen : list) {
            String time= jiFen.getTime();
            SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date= null;
            try {
                //将日期格式的字符串-->日期对象
                date = formatter.parse(time);
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
            Date a = new Date(System.currentTimeMillis());
            long l = a.getTime() - date.getTime();
            if((l/(1000*60*60*24))<31){
                sum+=jiFen.getJifen();
            }
        }
        return sum;
    }
    @Override
    public int addjifen(JiFen jiFen){
        SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date a = new Date(System.currentTimeMillis());
        String format = formatter.format(a);
        jiFen.setTime(format);
        return userMapper.addjifen(jiFen);
    }
    @Override
    public Object updatejifen(JiFen jiFen){
        List<JiFen> list = userMapper.getjifen(jiFen.getUserId());
        Double use =jiFen.getJifen();
        for (JiFen jiFen1 : list) {
            int id=jiFen1.getId();
            Double jifen = jiFen.getJifen();
            if(use>jifen)
            {
                use=use-jifen;
                int updatajifen = userMapper.deletejifen(id);


            }else {
                jifen=jifen-use;
                return userMapper.updatejifen(jiFen);
            }
        }
        return null;
    }
}