package com.neusoft.elmboot.service;
import com.neusoft.elmboot.po.JiFen;
import com.neusoft.elmboot.po.User;

public interface UserService {
    public User getUserByIdByPass(User user);
    public int getUserById(String userId);
    public int saveUser(User user);
    public int getjifen(String userId);
    public  int addjifen(JiFen jiFen);
    public Object updatejifen(JiFen jiFen);
}