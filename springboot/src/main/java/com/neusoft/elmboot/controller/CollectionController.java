package com.neusoft.elmboot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.neusoft.elmboot.po.Business;
import com.neusoft.elmboot.po.Collections;
import com.neusoft.elmboot.service.CollectionService;

@RestController
@RequestMapping("/CollectionController")
public class CollectionController {

    @Autowired
    private CollectionService collectionService;

    @RequestMapping("/saveCollection")
    public int saveCollection(Collections collection) throws Exception{
        return collectionService.saveCollection(collection);
    }
    @RequestMapping("/deleteCollection")
    public int deleteCollection(Collections collection) throws Exception{
        return collectionService.deleteCollection(collection);
    }
    @RequestMapping("/listCollection")
    public List<Business> listCollection(Collections collection) throws Exception{
        return collectionService.listCollection(collection.getUserId());
    }
    @RequestMapping("/getCollection")
    public int getCollection(Collections collection) throws Exception{
        return collectionService.getCollection(collection);
    }
}
