package com.neusoft.elmboot.po;

public class Collections {

    private Integer collectId;
    private Integer businessId;
    private String userId;
    private Integer delTag;

    public Integer getCollectId() {
        return collectId;
    }
    public void setCollectId(Integer collectId) {
        this.collectId = collectId;
    }
    public Integer getBusinessId() {
        return businessId;
    }
    public void setBusinessId(Integer businessId) {
        this.businessId = businessId;
    }

    public Integer getDelTag() {
        return delTag;
    }
    public void setDelTag(Integer delTag) {
        this.delTag = delTag;
    }
    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }


}
