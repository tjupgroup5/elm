package com.neusoft.elmboot.po;

import java.util.Date;

public class JiFen {
    private  String userId;
    private String Time;
    private Double jifen;
    private Integer total;
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTime() {
        return Time;
    }
    public void setTime(String time) {
        this.Time = time;
    }

    public Double getJifen() {
        return jifen;
    }

    public void setJifen(Double jifen) {
        this.jifen = jifen;
    }

}
