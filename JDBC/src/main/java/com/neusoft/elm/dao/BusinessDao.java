package com.neusoft.elm.dao;

import com.neusoft.elm.po.Business;

import java.util.List;

public interface BusinessDao {
    public List<Business> listBusiness(String businessName, String businessAddress);
    public int addBusiness(String businessName);
    public int delBusiness(int businessId);
    public Business getBusinessByIdAndPassword(int businessId, String password);
    public Business getBusinessById (int businessId);
    public int updateBusiness(Business business);
    public int updatePassword(int businessId, String password);
}
