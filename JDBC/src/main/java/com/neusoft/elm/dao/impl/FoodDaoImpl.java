package com.neusoft.elm.dao.impl;

import com.neusoft.elm.dao.FoodDao;
import com.neusoft.elm.po.Business;
import com.neusoft.elm.po.Food;
import com.neusoft.elm.util.DBUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class FoodDaoImpl implements FoodDao {
    private int businessId;
    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    public FoodDaoImpl (int id) {
        businessId = id;
    }
    @Override
    public List<Food> listFood() {
        List<Food> foodList = new ArrayList<>();
        String sql = new String("select * from food where businessId = ? order by foodId");
        try {
            con = DBUtil.getConnection();
            ps = con.prepareStatement(sql.toString());
            ps.setInt(1, businessId);
            rs = ps.executeQuery();
            while (rs.next()) {
                Food food = new Food();
                food.setFoodId(rs.getInt("foodId"));
                food.setFoodName(rs.getString("foodName"));
                food.setFoodExplain(rs.getString("foodExplain"));
                food.setFoodPrice(rs.getDouble("foodPrice"));
                food.setBusinessId(businessId);
                foodList.add(food);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            DBUtil.close(rs, ps, con);
        }
        return foodList;
    }

    @Override
    public int addFood(String foodName) {
        String sql = "insert into food(foodName, businessId) values(?, ?)";
        int id = 0;
        try {
            con = DBUtil.getConnection();
            ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, foodName);
            ps.setInt(2, businessId);
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            if (rs.next())	id = rs.getInt(1);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            DBUtil.close(rs, ps, con);
        }
        return id;
    }

    @Override
    public int updateFood(Food food) {
        String sql = "update food set foodName = ?, foodExplain = ?, foodPrice = ? where foodId = ? and businessId = ?";
        int num = 0;
        try {
            con = DBUtil.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, food.getFoodName());
            ps.setString(2, food.getFoodExplain());
            ps.setDouble(3, food.getFoodPrice());
            ps.setInt(4, food.getFoodId());
            ps.setInt(5, food.getBusinessId());
            num = ps.executeUpdate();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            DBUtil.close(null, ps, con);
        }
        return num;
    }

    @Override
    public int delFood(int foodId) {
        String sql = "delete from food where foodId = ? and businessId = ?";
        int num = 0;
        try {
            con = DBUtil.getConnection();
            ps = con.prepareStatement(sql);
            ps.setInt(1, foodId);
            ps.setInt(2, businessId);
            num = ps.executeUpdate();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            DBUtil.close(null, ps, con);
        }
        return num;
    }

    @Override
    public Food getFoodById(int foodId) {
        Food food = null;
        String sql = "select * from food where foodId = ? and businessId = ?";
        try {
            con = DBUtil.getConnection();
            ps = con.prepareStatement(sql);
            ps.setInt(1, foodId);
            ps.setInt(2, businessId);
            rs = ps.executeQuery();
            if (rs.next()) {
                food = new Food();
                food.setFoodId(foodId);
                food.setFoodName(rs.getString("foodName"));
                food.setFoodExplain(rs.getString("foodExplain"));
                food.setFoodPrice(rs.getDouble("foodPrice"));
                food.setBusinessId(rs.getInt("businessId"));
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            DBUtil.close(rs, ps, con);
        }
        return food;
    }
}
