package com.neusoft.elm.dao.impl;

import com.neusoft.elm.dao.BusinessDao;
import com.neusoft.elm.po.Admin;
import com.neusoft.elm.po.Business;
import com.neusoft.elm.util.DBUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class BusinessDaoImpl implements BusinessDao {
    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    @Override
    public List<Business> listBusiness(String businessName, String businessAddress) {
        List<Business> businessList = new ArrayList<>();
        StringBuffer sql = new StringBuffer("select * from business where true");
        if (businessName != null && !businessName.equals("")) {
            sql.append(" and businessName like '%" + businessName + "%'");
        }
        if (businessAddress !=null && !businessAddress.equals("`")) {
            sql.append(" and businessAddress like '%" + businessAddress + "%'");
        }
        sql.append(" order by businessId");
        try {
            con = DBUtil.getConnection();
            ps = con.prepareStatement(sql.toString());
            rs = ps.executeQuery();
            while (rs.next()) {
                Business business = new Business();
                business.setBusinessId(rs.getInt("businessId"));
                business.setPassword(rs.getString("password"));
                business.setBusinessName(rs.getString("businessName"));
                business.setBusinessAddress(rs.getString("businessAddress"));
                business.setBusinessExpalin(rs.getString("businessExplain"));
                business.setStarPrice(rs.getDouble("starPrice"));
                business.setDeliveryPrice(rs.getDouble("deliveryPrice"));
                businessList.add(business);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            DBUtil.close(rs, ps, con);
        }
        return businessList;
    }

    @Override
    public int addBusiness(String businessName) {
        String sql = "insert into business(password, businessName) values('123', ?)";
        int id = 0;
        try {
            con = DBUtil.getConnection();
            ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, businessName);
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            if (rs.next())	id = rs.getInt(1);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            DBUtil.close(rs, ps, con);
        }
        return id;
    }

    @Override
    public int delBusiness(int businessId) {
        String sql = "delete from business where businessId = ?";
        int num = 0;
        try {
            con = DBUtil.getConnection();
            ps = con.prepareStatement(sql);
            ps.setInt(1, businessId);
            num = ps.executeUpdate();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            DBUtil.close(null, ps, con);
        }
        return num;
    }

    @Override
    public Business getBusinessByIdAndPassword(int businessId, String password) {
        Business business = null;
        String sql = "select * from business where businessId = ? and password = ?";
        try {
            con = DBUtil.getConnection();
            ps = con.prepareStatement(sql);
            ps.setInt(1, businessId);
            ps.setString(2, password);
            rs = ps.executeQuery();
            if (rs.next()) {
                business = new Business();
                business.setBusinessId(rs.getInt("businessId"));
                business.setBusinessName(rs.getString("businessName"));
                business.setBusinessAddress(rs.getString("businessAddress"));
                business.setBusinessExpalin(rs.getString("businessExplain"));
                business.setStarPrice(rs.getDouble("starPrice"));
                business.setDeliveryPrice(rs.getDouble("deliveryPrice"));
                business.setPassword(rs.getString("password"));
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            DBUtil.close(rs, ps, con);
        }
        return business;
    }

    @Override
    public Business getBusinessById(int businessId) {
        Business business = null;
        String sql = "select * from business where businessId = ?";
        try {
            con = DBUtil.getConnection();
            ps = con.prepareStatement(sql);
            ps.setInt(1, businessId);
            rs = ps.executeQuery();
            if (rs.next()) {
                business = new Business();
                business.setBusinessId(rs.getInt("businessId"));
                business.setBusinessName(rs.getString("businessName"));
                business.setBusinessAddress(rs.getString("businessAddress"));
                business.setBusinessExpalin(rs.getString("businessExplain"));
                business.setStarPrice(rs.getDouble("starPrice"));
                business.setDeliveryPrice(rs.getDouble("deliveryPrice"));
                business.setPassword(rs.getString("password"));
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            DBUtil.close(rs, ps, con);
        }
        return business;
    }

    @Override
    public int updateBusiness(Business business) {
        String sql = "update business set businessName = ?, businessAddress = ?, businessExplain = ?, starPrice = ?, deliveryPrice = ? where businessId = ?";
        int num = 0;
        try {
            con = DBUtil.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, business.getBusinessName());
            ps.setString(2, business.getBusinessAddress());
            ps.setString(3, business.getBusinessExpalin());
            ps.setDouble(4, business.getStarPrice());
            ps.setDouble(5, business.getDeliveryPrice());
            ps.setInt(6, business.getBusinessId());
            num = ps.executeUpdate();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            DBUtil.close(null, ps, con);
        }
        return num;
    }

    @Override
    public int updatePassword(int businessId, String password) {
        String sql = "update business set password = ? where businessId = ?";
        int num = 0;
        try {
            con = DBUtil.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, password);
            ps.setInt(2, businessId);
            num = ps.executeUpdate();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            DBUtil.close(null, ps, con);
        }
        return num;
    }
}
