package com.neusoft.elm.dao;

import com.neusoft.elm.po.Business;
import com.neusoft.elm.po.Food;

import java.util.List;

public interface FoodDao {
    List<Food> listFood ();
    public int addFood(String foodName);
    public int updateFood(Food food);
    public int delFood(int foodId);
    public Food getFoodById (int foodId);
}
