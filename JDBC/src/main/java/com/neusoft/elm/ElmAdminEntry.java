package com.neusoft.elm;

import com.neusoft.elm.po.Admin;
import com.neusoft.elm.po.Business;
import com.neusoft.elm.view.AdminView;
import com.neusoft.elm.view.BusinessView;
import com.neusoft.elm.view.impl.AdminViewImpl;
import com.neusoft.elm.view.impl.BusinessViewImpl;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ElmAdminEntry {
    public void work () {
        Scanner scanner = new Scanner(System.in);
        System.out.println("----------------------------------------------------------------");
        System.out.println("|\t\t\t\t\t\t 饿了么后台管理系统 \t\t\t\t\t\t|");
        System.out.println("----------------------------------------------------------------");
        AdminView adminView = new AdminViewImpl();
        Admin admin = adminView.Login();
        BusinessView businessView = new BusinessViewImpl();
        // 登录成功
        if (admin != null) {
            int opt = 0;
            while (opt != 5) {
                System.out.println("\n=========1.商家列表=2.搜索商家=3.新建商家=4.删除商家=5.退出系统=========");
                System.out.println("请输入你的选择(一个1~5的数字，输入完请回车)：");
                try {
                    opt = scanner.nextInt();
                    switch (opt) {
                        case 1:
                            businessView.listBusinessAll();
                            break;
                        case 2:
                            businessView.searchBusiness();
                            break;
                        case 3:
                            businessView.addBusiness();
                            break;
                        case 4:
                            businessView.delBusiness();
                            break;
                        case 5:
                            System.out.println("--------------------欢迎下次光临饿了么后台系统-------------\n");
                            break;
                        default:
                            System.out.println("请输入有效数字!");
                    }
                }
                catch (InputMismatchException e) {
                    System.out.println("请输入有效数字!");
                    opt = 0;
                }
                scanner.nextLine();
            }
        }
        else {
            System.out.println("\n管理员名称或者密码错误!\n");
        }
    }
    public static void main (String[] args) {
        new ElmAdminEntry().work();
    }
}

