package com.neusoft.elm.view.impl;

import com.neusoft.elm.dao.BusinessDao;
import com.neusoft.elm.dao.impl.BusinessDaoImpl;
import com.neusoft.elm.po.Business;
import com.neusoft.elm.view.BusinessView;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class BusinessViewImpl implements BusinessView {
    @Override
    public void listBusinessAll() {
        List<Business> businessList = new BusinessDaoImpl().listBusiness(null, null);
        System.out.println("商家编号\t商家名称\t商家地址\t商家介绍\t起送费\t配送费");
        for (Business business : businessList)
            System.out.println(business.getBusinessId() + "\t" + business.getBusinessName() + "\t" + business.getBusinessAddress() + "\t" + business.getBusinessExpalin() + "\t" + business.getStarPrice() + "\t" + business.getDeliveryPrice());
    }

    @Override
    public void searchBusiness() {
        Scanner  scanner = new Scanner(System.in);
        String businessName = null, businessAddress = null;
        System.out.println("是否搜索名称?(y/n)");
        String s;
        while (true) {
            s = scanner.next();
            if ("y".equals(s.toLowerCase())) {
                System.out.println("请输入商家名称：");
                businessName = scanner.next();
                break;
            } else if ("n".equals(s.toLowerCase())) break;
            else	System.out.println("请输入y 或者 n!");
            scanner.nextLine();
        }
        System.out.println("是否搜索地址?(y/n)");
        while (true) {
            s = scanner.next();
            if ("y".equals(s.toLowerCase())) {
                System.out.println("请输入商家地址：");
                businessAddress = scanner.next();
                break;
            } else if ("n".equals(s.toLowerCase())) break;
            else	System.out.println("请输入y 或者 n!");
            scanner.nextLine();
        }
        List <Business> businessList = new BusinessDaoImpl().listBusiness(businessName, businessAddress);
        System.out.println("商家编号\t商家名称\t商家地址\t商家介绍\t起送费\t配送费");
        for (Business business : businessList)
            System.out.println(business.getBusinessId() + "\t" + business.getBusinessName() + "\t" + business.getBusinessAddress() + "\t" + business.getBusinessExpalin() + "\t" + business.getStarPrice() + "\t" + business.getDeliveryPrice());
    }

    @Override
    public void addBusiness() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入商家名称：");
        String name = scanner.next();
        Integer id = new BusinessDaoImpl().addBusiness(name);
        if (id == null || id.intValue() == 0)    System.out.println("创建商家失败！");
        else	System.out.println("创建商家成功！商家编号为：" + id.toString());
    }

    @Override
    public void delBusiness() {
        Scanner scanner = new Scanner(System.in);
        boolean flag = true;
        while (flag) {
            System.out.println("请输入商家编号：(输入n退出删除)");
            try {
                int id = scanner.nextInt();
                System.out.println("确认删除吗？(y/n)");
                while (true) {
                    String s = scanner.next();
                    if ("y".equals(s.toLowerCase())) {
                        int num = new BusinessDaoImpl().delBusiness(id);
                        if (num == 0)	System.out.println("该编号商家不存在！");
                        else    System.out.println("删除商家成功！");
                        break;
                    }
                    else if ("n".equals(s.toLowerCase())) {
                        System.out.println("已取消删除");
                        break;
                    }
                    else {
                        System.out.println("请输入 y 或者 n!");
                        scanner.nextLine();
                    }
                }
            } catch (InputMismatchException e) {
                String s = scanner.next();
                if ("n".equals(s.toLowerCase()))  {
                    System.out.println("已退出删除");
                    flag = false;
                }
                else {
                    System.out.println("请输入一个数字！");
                    scanner.nextLine();
                }
            }
        }
    }

    @Override
    public Business login() {
        Scanner input = new Scanner(System.in);
        System.out.println("请输入商家编号：");
        int businessId = input.nextInt();
        System.out.println("请输入商家密码：");
        String password = input.next();
        return new BusinessDaoImpl().getBusinessByIdAndPassword(businessId, password);
    }

    @Override
    public void showBusinss(int businessId) {
        Business business = new BusinessDaoImpl().getBusinessById(businessId);
        System.out.println(business);
    }

    @Override
    public void updateBusiness(int businessId) {
        Scanner input = new Scanner(System.in);
        BusinessDao businessDao = new BusinessDaoImpl();
        Business business = businessDao.getBusinessById(businessId);
        System.out.println(business);
        String s;
        Double p;
        System.out.println("是否修改商家名称(y/n)：");
        s = input.next();
        if ("y".equals(s)) {
            System.out.println("请输入新的商家名称：");
            s = input.next();
            business.setBusinessName(s);
        }
        System.out.println("是否修改商家地址(y/n)：");
        s = input.next();
        if ("y".equals(s)) {
            System.out.println("请输入新的商家地址：");
            s = input.next();
            business.setBusinessAddress(s);
        }
        System.out.println("是否修改商家介绍(y/n)：");
        s = input.next();
        if ("y".equals(s)) {
            System.out.println("请输入新的商家介绍：");
            s = input.next();
            business.setBusinessExpalin(s);
        }
        System.out.println("是否修改起送费(y/n)：");
        s = input.next();
        if ("y".equals(s)) {
            System.out.println("请输入新的起送费：");
            p = input.nextDouble();
            business.setStarPrice(p);
        }
        System.out.println("是否修改配送费(y/n)：");
        s = input.next();
        if ("y".equals(s)) {
            System.out.println("请输入新的配送费：");
            p = input.nextDouble();
            business.setDeliveryPrice(p);
        }
        int num = businessDao.updateBusiness(business);
        if (num > 0)    System.out.println("商家信息更新成功！");
        else    System.out.println("商家信息更新失败！");
    }

    @Override
    public void updatePassword(int businessId) {
        Scanner input = new Scanner(System.in);
        BusinessDao businessDao = new BusinessDaoImpl();
        String old, new1, new2;
        System.out.println("请输入旧密码：");
        old = input.next();
        System.out.println("请输入新密码：");
        new1 = input.next();
        System.out.println("请再次输入新密码：");
        new2 = input.next();
        if (new1.equals(new2)) {
            Business business = businessDao.getBusinessByIdAndPassword(businessId, old);
            if (business != null) {
                int num = businessDao.updatePassword(businessId, new1);
                if (num > 0)    System.out.println("密码更新成功！");
                else    System.out.println("密码更新失败！");
            }
            else {
                System.out.println("旧密码错误！");
            }
        }
        else {
            System.out.println("两次密码输入不一致！");
        }
    }
}
