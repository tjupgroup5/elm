package com.neusoft.elm.view;

import com.neusoft.elm.po.Business;

public interface BusinessView {
    public void listBusinessAll ();
    public void searchBusiness ();
    public void addBusiness();
    public void delBusiness();
    public Business login ();
    public void showBusinss (int businessId);
    public void updateBusiness(int businessId);
    public void updatePassword (int businessId);
}
