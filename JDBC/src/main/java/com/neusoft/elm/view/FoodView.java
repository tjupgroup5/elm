package com.neusoft.elm.view;

public interface FoodView {
    public void listFood ();
    public void addFood ();
    public void updateFood();
    public void delFood();
}
