package com.neusoft.elm.view.impl;

import com.neusoft.elm.dao.BusinessDao;
import com.neusoft.elm.dao.FoodDao;
import com.neusoft.elm.dao.impl.BusinessDaoImpl;
import com.neusoft.elm.dao.impl.FoodDaoImpl;
import com.neusoft.elm.po.Business;
import com.neusoft.elm.po.Food;
import com.neusoft.elm.view.FoodView;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class FoodViewImpl implements FoodView {
    private int businessId;
    public FoodViewImpl(int id) {
        businessId = id;
    }
    @Override
    public void listFood() {
        List<Food> foodList = new FoodDaoImpl(businessId).listFood();
        System.out.println("食品编号\t食品名称\t食品介绍\t食品价格");
        for (Food food : foodList)
            System.out.println(food.getFoodId() + "\t" + food.getFoodName() + "\t" + food.getFoodExplain() + "\t" + food.getFoodPrice());
    }

    @Override
    public void addFood() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入食品名称：");
        String name = scanner.next();
        Integer id = new FoodDaoImpl(businessId).addFood(name);
        if (id == null || id.intValue() == 0)    System.out.println("新增食品失败！");
        else	System.out.println("新增食品成功！食品编号为：" + id.toString());
    }

    @Override
    public void updateFood() {
        Scanner input = new Scanner(System.in);
        FoodDao foodDao = new FoodDaoImpl(businessId);
        int foodId = 0;
        listFood();
        System.out.println("请输入所要修改的食品编号");
        foodId = input.nextInt();
        Food food = foodDao.getFoodById(foodId);
        System.out.println(food);
        String s;
        Double p;
        System.out.println("是否修改食品名称(y/n)：");
        s = input.next();
        if ("y".equals(s)) {
            System.out.println("请输入新的食品名称：");
            s = input.next();
            food.setFoodName(s);
        }
        System.out.println("是否修改食品介绍(y/n)：");
        s = input.next();
        if ("y".equals(s)) {
            System.out.println("请输入新的食品介绍：");
            s = input.next();
            food.setFoodExplain(s);
        }
        System.out.println("是否修改食品价格(y/n)：");
        s = input.next();
        if ("y".equals(s)) {
            System.out.println("请输入新的食品价格：");
            p = input.nextDouble();
            food.setFoodPrice(p);
        }
        int num = foodDao.updateFood(food);
        if (num > 0)    System.out.println("食品信息更新成功！");
        else    System.out.println("食品信息更新失败！");
    }

    @Override
    public void delFood() {
        Scanner scanner = new Scanner(System.in);
        boolean flag = true;
        while (flag) {
            System.out.println("请输入食品编号：(输入n退出删除)");
            try {
                int id = scanner.nextInt();
                System.out.println("确认删除吗？(y/n)");
                while (true) {
                    String s = scanner.next();
                    if ("y".equals(s.toLowerCase())) {
                        int num = new FoodDaoImpl(businessId).delFood(id);
                        if (num == 0)	System.out.println("该编号食品不存在！");
                        else    System.out.println("删除食品成功！");
                        break;
                    }
                    else if ("n".equals(s.toLowerCase())) {
                        System.out.println("已取消删除");
                        break;
                    }
                    else {
                        System.out.println("请输入 y 或者 n!");
                        scanner.nextLine();
                    }
                }
            } catch (InputMismatchException e) {
                String s = scanner.next();
                if ("n".equals(s.toLowerCase()))  {
                    System.out.println("已退出删除");
                    flag = false;
                }
                else {
                    System.out.println("请输入一个数字！");
                    scanner.nextLine();
                }
            }
        }
    }
}
