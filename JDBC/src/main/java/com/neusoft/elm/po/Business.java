package com.neusoft.elm.po;

public class Business {
    private Integer businessId;
    private String password, businessName, businessAddress, businessExpalin;
    private Double starPrice, deliveryPrice;
    @Override
    public String toString () {
        return "\n商家编号： " + this.businessId +
               "\n商家名称： " + this.businessName +
               "\n商家地址： " + this.businessAddress +
               "\n商家介绍： " + this.businessExpalin +
               "\n起送费： " + this.starPrice +
               "\n配送费： " + this.deliveryPrice;
    }
    public Integer getBusinessId() {
        return businessId;
    }

    public void setBusinessId(Integer businessId) {
        this.businessId = businessId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getBusinessExpalin() {
        return businessExpalin;
    }

    public void setBusinessExpalin(String businessExpalin) {
        this.businessExpalin = businessExpalin;
    }

    public Double getStarPrice() {
        return starPrice;
    }

    public void setStarPrice(Double starPrice) {
        this.starPrice = starPrice;
    }

    public Double getDeliveryPrice() {
        return deliveryPrice;
    }

    public void setDeliveryPrice(Double deliveryPrice) {
        this.deliveryPrice = deliveryPrice;
    }
}
