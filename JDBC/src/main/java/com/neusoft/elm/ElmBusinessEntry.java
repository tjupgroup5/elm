package com.neusoft.elm;

import com.neusoft.elm.po.Business;
import com.neusoft.elm.view.BusinessView;
import com.neusoft.elm.view.FoodView;
import com.neusoft.elm.view.impl.BusinessViewImpl;
import com.neusoft.elm.view.impl.FoodViewImpl;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ElmBusinessEntry {
    public void manageFood (int businessId) {
        Scanner scanner = new Scanner(System.in);
        FoodView foodView = new FoodViewImpl(businessId);
        int opt = 0;
        while (opt != 5) {
            System.out.println("\n=========二级菜单（商品管理）1.查看食品列表=2.新增食品=3.修改食品=4.删除食品=5.返回一级菜单=========");
            System.out.println("请输入你的选择(一个1~5的数字，输入完请回车)：");
            try {
                opt = scanner.nextInt();
                switch (opt) {
                    case 1:
                        foodView.listFood();
                        break;
                    case 2:
                        foodView.addFood();
                        break;
                    case 3:
                        foodView.updateFood();
                        break;
                    case 4:
                        foodView.delFood();
                        break;
                    case 5:
                        System.out.println("-------------------欢迎下次光临饿了么食品后台系统------------\n");
                        break;
                    default:
                        System.out.println("请输入有效数字!");
                }
            }
            catch (InputMismatchException e) {
                System.out.println("请输入有效数字!");
                opt = 0;
            }
            scanner.nextLine();
        }
    }
    public void work () {
        Scanner scanner = new Scanner(System.in);
        System.out.println("----------------------------------------------------------------");
        System.out.println("|\t\t\t\t\t\t 饿了么后台管理系统 \t\t\t\t\t\t|");
        System.out.println("----------------------------------------------------------------");
        BusinessView businessView = new BusinessViewImpl();
        Business business = businessView.login();
        if (business != null) {
            int opt = 0;
            while (opt != 5) {
                System.out.println("\n=========一级菜单（商家管理）1.查看商家信息=2.修改商家信息=3.更新密码=4.所属商品管理=5.退出系统=========");
                System.out.println("请输入你的选择(一个1~5的数字，输入完请回车)：");
                try {
                    opt = scanner.nextInt();
                    switch (opt) {
                        case 1:
                            businessView.showBusinss(business.getBusinessId());
                            break;
                        case 2:
                            businessView.updateBusiness(business.getBusinessId());
                            break;
                        case 3:
                            businessView.updatePassword(business.getBusinessId());
                            break;
                        case 4:
                            manageFood(business.getBusinessId());
                            break;
                        case 5:
                            System.out.println("--------------------欢迎下次光临饿了么后台系统-------------\n");
                            break;
                        default:
                            System.out.println("请输入有效数字!");
                    }
                }
                catch (InputMismatchException e) {
                    System.out.println("请输入有效数字!");
                    opt = 0;
                }
                scanner.nextLine();
            }
        }
        else System.out.println("\n商家名称或密码错误!");
    }

    public static void main(String[] args) {
        new ElmBusinessEntry().work();
    }
}
